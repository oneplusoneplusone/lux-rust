# LUX

**Lux** is a small CLI application written in Rust
that allows you to generate sound from pictures.

# Usage

`lux <path/to/your/image/file> <path/to/your/sound/file>`

# How it Works

The algorithm considers the input image as a
[spectrogram](https://en.wikipedia.org/wiki/Spectrogram)
of your sound.
It then rebuilds the sound using the
[Inverse Fast Fourier Transform](https://en.wikipedia.org/wiki/Fast_Fourier_transform).