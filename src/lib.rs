use std::error::Error;
use std::fs;

mod core;

pub fn run(config: &core::config::Config) -> Result<(), Box<dyn Error>> {
	let begin = std::time::SystemTime::now();
	let sound_path = config.sound_path().to_string();

	fs::create_dir_all("exports").expect("could not create exports directory!");

	println!("Slicing image into iffts...");
	let image = core::image::import::ImageImporter::new(config.image_path().to_string());
	let mut waveform = core::audio::waveform::Waveform::new();
	let width = image.width().clone();
	for x in 0..width {
		let slice = &image.get_slice(x);
		waveform.add_frequential(slice.to_vec());
	}
	println!("Generating audio signal...");
	waveform.update();

	println!("Exporting Wav file..");
	let output = waveform.temporal();
	core::audio::export::wav_export::WavExport::new().export(&output, &sound_path);

	if config.gen_image() {
		println!("Exporting Png...");
		let image_out: String = sound_path.replace(".*", ".png");
		core::image::export::ImageExporter::new(1.1).export_signal(&output, &image_out);
	}

	match begin.elapsed() {
		Ok(elapsed) => {
			println!("Took {}ms to process", elapsed.as_millis());
		},
		Err(e) => {
			println!("Error: {:?}", e);
		}
	}

	Ok(())
}

pub fn parse_config(args: std::env::Args) -> Result<core::config::Config, &'static str> {
	core::config::Config::new(args)
}
