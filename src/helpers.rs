fn create_sine_fft(num_samples: usize, frequency: usize) -> dsp::ComplexBuffer
{
	assert!(frequency < num_samples);
	let mut input = vec![num_complex::Complex32::new(0.0, 0.0); num_samples];
	input[frequency] = num_complex::Complex32::new(0.4, 0.0);
	
	return input;
}

#[cfg(test)]
mod tests
{
	use super::*;

	#[test]
	fn sine_from_one_sample_fft() {
			assert_eq!(create_sine_fft(1,0).len(), 1);
			assert_eq!(create_sine_fft(1,0)[0], num_complex::Complex32::new(0.4, 0.0));
	}
}