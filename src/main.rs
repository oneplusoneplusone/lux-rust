use std::env;
use std::process;

fn main() {
	let config = lux::parse_config(env::args()).unwrap_or_else(|err| {
		eprintln!("Error while parsing arguments: {}", err);
		process::exit(1);
	});

	if let Err(e) = lux::run(&config) {
		eprintln!("Application error: {}", e);
		process::exit(1);
	}
}
