use dsp;
use hound;

pub struct WavExport {
	spec: hound::WavSpec,
}

impl WavExport {
	pub fn new() -> WavExport {
		let spec = hound::WavSpec {
			channels: 2,
			sample_rate: 44100,
			bits_per_sample: 16,
			sample_format: hound::SampleFormat::Int,
		};
		return WavExport { spec };
	}

	pub fn export(self, signal: &dsp::ComplexBuffer, file_name: &String) {
		let output_path = file_name;
		let mut writer = hound::WavWriter::create(output_path, self.spec).unwrap();

		for sample in signal {
			let amplitude = std::i16::MAX as f32;
			for _ in 0..self.spec.channels {
				writer.write_sample((sample.re * amplitude) as i16).unwrap();
			}
		}
	}
}
