use dsp::fft;

pub struct Waveform {
	frequentials: Vec<dsp::ComplexBuffer>,
	temporal: dsp::ComplexBuffer,
}

impl Waveform {
	pub fn new() -> Waveform {
		let frequentials = Vec::new();
		let temporal = dsp::ComplexBuffer::new();
		return Waveform {
			frequentials,
			temporal,
		};
	}

	pub fn add_frequential(&mut self, frequential: dsp::ComplexBuffer) {
		self.frequentials.push(frequential);
	}

	pub fn temporal(self) -> dsp::ComplexBuffer {
		self.temporal
	}

	pub fn update(&mut self) {
		self.temporal = Vec::new();

		for frequential in &self.frequentials {
			let mut ifft = fft::InverseFFT::new(frequential.len());
			let mut temporal = vec![num_complex::Complex32::new(0.0, 0.0); frequential.len()];
			ifft.process(&mut frequential.clone(), &mut temporal);
			for value in temporal {
				self.temporal.push(value);
			}
		}

		self.trim();
	}

	fn max(buffer: &dsp::ComplexBuffer) -> num_complex::Complex32 {
		let mut max = num_complex::Complex32::new(0.0, 0.0);

		for val in buffer {
			if val.re > max.re {
				max = *val;
			}
		}

		return max;
	}

	fn trim(&mut self) {
		let max = Waveform::max(&self.temporal);

		if max.re <= 1.0 {
			return;
		}

		for (_, value) in self.temporal.iter_mut().enumerate() {
			value.im = 0.0;
			value.re /= max.re;
		}
	}
}

#[cfg(test)]
mod tests {
	use rand::Rng;

	use super::*;

	fn generate_random_signal(size: usize, max_val: f32) -> dsp::ComplexBuffer {
		let mut rng = rand::thread_rng();
		let mut signal = dsp::ComplexBuffer::new();
		for _ in 0..size {
			signal.push(num_complex::Complex32::new(
				rng.gen_range(0.0, max_val),
				rng.gen_range(0.0, max_val),
			));
		}

		signal
	}

	#[test]
	fn trimmed_output_amplitude_should_never_exceed_one() {
		let mut waveform = Waveform::new();
		waveform.temporal = generate_random_signal(25, 10.0);
		waveform.trim();
		let max_amp = 1.0;

		for (_, value) in waveform.temporal.iter().enumerate() {
			assert!(max_amp >= value.re);
		}
	}

	#[test]
	fn trim_of_0_filled_signal_should_return_signal() {
		let mut waveform = Waveform::new();
		waveform.temporal = vec![num_complex::Complex32::new(0.0, 10.0); 5];
		let expected = waveform.temporal.clone();

		waveform.trim();

		assert_eq!(expected, waveform.temporal);
	}
}
