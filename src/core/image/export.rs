use plotters::prelude::*;

pub struct ImageExporter {
	range: f32,
}

impl ImageExporter {
	pub fn new(range: f32) -> ImageExporter {
		return ImageExporter { range };
	}

	pub fn export_signal(self, time_signal: &dsp::ComplexBuffer, file_name: &String) {
		let full_path = format!("{}", file_name);
		let root_area = BitMapBackend::new(&full_path, (600, 400)).into_drawing_area();
		root_area.fill(&WHITE).unwrap();

		let buffer_size = time_signal.len();

		let mut context = ChartBuilder::on(&root_area)
			.set_label_area_size(LabelAreaPosition::Left, 40)
			.set_label_area_size(LabelAreaPosition::Bottom, 40)
			.caption("Output Signal", ("Arial", 40))
			.build_ranged(
				0f32..buffer_size as f32,
				-self.range as f32..self.range as f32,
			)
			.unwrap();

		context.configure_mesh().draw().unwrap();

		let series = LineSeries::new(
			(0..buffer_size as usize).map(|x| (x as f32, time_signal[x].re as f32)),
			&GREEN,
		);

		context.draw_series(series).unwrap();
	}
}
