extern crate image;

use image::GenericImageView;
use num_complex;

pub struct ImageImporter {
	image: image::DynamicImage,
}

impl ImageImporter {
	pub fn new(path: String) -> ImageImporter {
		let image = image::open(path).unwrap();
		return ImageImporter { image };
	}

	pub fn width(&self) -> usize {
		return self.image.dimensions().0 as usize;
	}

	fn get_real_part(&self, x: u32, y: u32) -> f32 {
		let pixel = &self.image.get_pixel(x as u32, y as u32);
		let r = pixel[0] as u16;
		let g = pixel[1] as u16;
		let b = pixel[2] as u16;
		let rgb_sum = r + g + b;
		let rgb_mean = rgb_sum / 3;
		return (rgb_mean as f32) / 255.0;
	}

	pub fn get_slice(&self, x: usize) -> Vec<num_complex::Complex32> {
		let (_width, height) = self.image.dimensions();
		let mut slice = vec![num_complex::Complex32::new(0.0, 0.0); height as usize];
		for y in 0..height {
			let real_part = self.get_real_part(x as u32, y as u32);
			slice[y as usize] = num_complex::Complex32::new(real_part, 0.0);
		}

		return slice;
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn slice_of_black_image_should_give_0_filled_fft() {
		let image = ImageImporter::new("src/core/image/black_col_1_28.png".to_string());
		let (_width, height) = image.image.dimensions();
		let expected = vec![num_complex::Complex32::new(0.0, 0.0); height as usize];
		assert_eq!(expected, image.get_slice(0));
	}

	#[test]
	fn slice_of_white_image_should_give_1_filled_fft() {
		let image = ImageImporter::new("src/core/image/white_col_1_28.png".to_string());
		let (_width, height) = image.image.dimensions();
		let expected = vec![num_complex::Complex32::new(1.0, 0.0); height as usize];
		println!("{:?}", image.get_slice(0));
		assert_eq!(expected, image.get_slice(0));
	}
}
