pub struct Config {
	image_path: String,
	sound_path: String,
	gen_image: bool,
}

impl Config {
	pub fn new<T>(mut args: T) -> Result<Config, &'static str>
	where
		T: Iterator<Item = String>,
	{
		args.next();

		let image_path = match args.next() {
			Some(arg) => arg,
			None => return Err("Didn't receive image path!"),
		};

		let sound_path = match args.next() {
			Some(arg) => arg,
			None => return Err("Didn't receive sound path!"),
		};

		let gen_image = args.into_iter().any(|x| x == "--gen-image");

		Ok(Config {
			image_path,
			sound_path,
			gen_image,
		})
	}

	pub fn image_path(&self) -> &String {
		&self.image_path
	}

	pub fn sound_path(&self) -> &String {
		&self.sound_path
	}

	pub fn gen_image(&self) -> bool {
		self.gen_image
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn adding_gen_image_option() {
		let args = ["", "image", "sound", "--gen-image"]
			.iter()
			.map(|s| s.to_string());
		let conf = Config::new(args).unwrap();
		assert_eq!("image", conf.image_path);
		assert_eq!("sound", conf.sound_path);
		assert!(conf.gen_image);
	}

	#[test]
	fn wrong_option() {
		let args = ["", "image", "sound", "wrong"]
			.iter()
			.map(|s| s.to_string());
		let conf = Config::new(args).unwrap();
		assert_eq!("image", conf.image_path);
		assert_eq!("sound", conf.sound_path);
		assert!(!conf.gen_image);
	}
}
